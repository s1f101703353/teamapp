package com.example.teamapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class AnotherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        ImageView img = findViewById(R.id.imageView);
        img.setImageResource(R.drawable.animal_stand_nezumi);
    }

}
